export class AppConstants {
  public static baseUrl = 'http://localhost:8080';

  public static routes = {
    home: '',
    pizza: 'pizza',
    addpizza: 'pizza/add',
    ordine: 'ordine',
    createordine: 'ordine/create'
  };

}
