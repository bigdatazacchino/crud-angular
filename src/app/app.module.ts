import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PizzaModule} from './pages/pizza/pizza.module';
import {AddPizzaModule} from './pages/pizza/add-pizza/add-pizza.module';
import {OrdineModule} from './pages/ordine/ordine.module';
import {CreateOrdineModule} from './pages/ordine/create-ordine/create-ordine.module';
import {RouterModule} from '@angular/router';
import {AppConstants} from './AppConstants';
import {HomeComponent} from './pages/home/home.component';
import {PizzaComponent} from './pages/pizza/pizza.component';
import {AddPizzaComponent} from './pages/pizza/add-pizza/add-pizza.component';
import {OrdineComponent} from './pages/ordine/ordine.component';
import {CreateOrdineComponent} from './pages/ordine/create-ordine/create-ordine.component';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    PizzaModule,
    AddPizzaModule,
    OrdineModule,
    CreateOrdineModule,
    RouterModule.forRoot([
      {path: AppConstants.routes.home, component: HomeComponent},
      {path: AppConstants.routes.pizza, component: PizzaComponent},
      {path: AppConstants.routes.addpizza, component: AddPizzaComponent},
      {path: AppConstants.routes.ordine, component: OrdineComponent},
      {path: AppConstants.routes.createordine, component: CreateOrdineComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent, NavbarComponent]
})
export class AppModule {
}
