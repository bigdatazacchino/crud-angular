import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrdineComponent } from './create-ordine.component';

describe('CreateOrdineComponent', () => {
  let component: CreateOrdineComponent;
  let fixture: ComponentFixture<CreateOrdineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateOrdineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrdineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
