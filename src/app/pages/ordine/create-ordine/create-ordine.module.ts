import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateOrdineComponent } from './create-ordine.component';



@NgModule({
  declarations: [CreateOrdineComponent],
  imports: [
    CommonModule
  ]
})
export class CreateOrdineModule { }
