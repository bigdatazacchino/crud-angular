import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdineComponent } from './ordine.component';



@NgModule({
  declarations: [OrdineComponent],
  imports: [
    CommonModule
  ]
})
export class OrdineModule { }
