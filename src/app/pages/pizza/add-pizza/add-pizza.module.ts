import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddPizzaComponent } from './add-pizza.component';



@NgModule({
  declarations: [AddPizzaComponent],
  imports: [
    CommonModule
  ]
})
export class AddPizzaModule { }
