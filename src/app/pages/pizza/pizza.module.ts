import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PizzaComponent } from './pizza.component';



@NgModule({
  declarations: [PizzaComponent],
  imports: [
    CommonModule
  ]
})
export class PizzaModule { }
